from django.urls import path
from cards.views import create_card, show_my_cards


# Tasks urls v
urlpatterns = [
    path("create/", create_card, name="create_card"),
    path("mine/", show_my_cards, name="show_my_cards"),
]
