from django import forms
from cards.models import Card


# Tasks forms v
class CardForm(forms.ModelForm):
    class Meta:
        model = Card
        exclude = ["is_completed", "owner"]
