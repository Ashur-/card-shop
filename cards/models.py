from django.db import models
from django.contrib.auth.models import User
from decks.models import Deck


# Cards Models v
class Card(models.Model):
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    deck = models.ForeignKey(
        Deck, on_delete=models.CASCADE, related_name="cards"
    )
    assignee = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="cards"
    )
    count = models.IntegerField(default=0)
    image = models.ImageField(upload_to='card_images/', default='default_image.jpg')

    def __str__(self):
        return self.name
    


# start_date = models.DateTimeField()
#     due_date = models.DateTimeField()
    # is_completed = models.BooleanField(default=False)