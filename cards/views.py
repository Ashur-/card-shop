from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from cards.forms import CardForm
from cards.models import Card


# Tasks views v
@login_required
def create_card(request):
    if request.method == "POST":
        form = CardForm(request.POST)
        if form.is_valid():
            card = form.save(commit=False)
            card.owner = request.user
            card.save()
            return redirect("list_decks")
    else:
        form = CardForm()
    return render(request, "cards/create.html", {"form": form})


@login_required
def show_my_cards(request):
    cards = Card.objects.filter(assignee=request.user)
    context = {"cards": cards}
    return render(request, "cards/my_cards.html", context)
