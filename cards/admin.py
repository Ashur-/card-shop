from django.contrib import admin
from cards.models import Card


# register models
admin.site.register(Card)
