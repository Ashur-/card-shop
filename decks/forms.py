from django import forms
from decks.models import Deck
from django.forms import ModelForm

class DeckForm(ModelForm):
    class Meta:
        model = Deck
        fields = ["name", "picture", "description", "price", "owner",]
        widgets = {
            'description': forms.Textarea(attrs={'rows': 3})
        }
