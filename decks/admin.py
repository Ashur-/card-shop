from django.contrib import admin
from decks.models import Deck

# register models
admin.site.register(Deck)
