from django.shortcuts import render, redirect, get_object_or_404
from decks.models import Deck
from django.contrib.auth.decorators import login_required
from django.db import models
from decks.forms import DeckForm
from django.shortcuts import render
from cards.models import Card

@login_required
def deck_list(request):
    decks = Deck.objects.filter(
        models.Q(owner=request.user)
        | models.Q(
            members=request.user
        )
    ).distinct()
    context = {"decks": decks}
    return render(request, "deck/list.html", context)


@login_required
def show_deck(request, id):
    deck = get_object_or_404(Deck, id=id)
    cards = deck.cards.all()
    context = {
        "deck": deck,
        "cards": cards,
        "deck_name": deck.name,
    }
    return render(request, "deck/show_deck.html", context)

# create decks by using card images from the card app
def create_deck(request):
    cards = Card.objects.all()
    context = {'cards': cards}
    return render(request, 'deck/create.html', context)