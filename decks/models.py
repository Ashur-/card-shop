from django.db import models
from django.contrib.auth.models import User
from django.forms import NumberInput

# create models v


class Deck(models.Model):
    name = models.CharField(max_length=200)
    picture = models.URLField(default=None)
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="decks", default=None
    )
    members = models.ManyToManyField(User, through="Membership")

    def __str__(self):
        return self.name


class Membership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    deck = models.ForeignKey(Deck, on_delete=models.CASCADE)
