from django.urls import path
from decks.views import deck_list, show_deck, create_deck

# patterns v
urlpatterns = [
    path("", deck_list, name="list_decks"),
    path("<int:id>/", show_deck, name="show_deck"),
    path("create/", create_deck, name="create_deck"),
]
