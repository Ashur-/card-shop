from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from .forms import LoginForm
from django.urls import reverse
from accounts.forms import SignupForm
from django.contrib.auth.models import User


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse("list_decks"))
            else:
                error_message = "Invalid username or password"
    else:
        form = LoginForm()
        error_message = ""
    return render(
        request,
        "accounts/login.html",
        {"form": form, "error_message": error_message},
    )


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password != password_confirmation:
                error_message = "The passwords do not match"
                return render(
                    request,
                    "registration/signup.html",
                    {"form": form, "error_message": error_message},
                )
            user = User.objects.create_user(
                username=username, password=password
            )
            login(request, user)
            return redirect(reverse("list_decks"))
    else:
        form = SignupForm()
    return render(request, "registration/signup.html", {"form": form})
