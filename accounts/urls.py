from django.urls import path
from accounts.views import login_view, logout_view, signup_view
from django.views.generic import TemplateView


urlpatterns = [
    path("login/", login_view, name="login"),
    path('accounts/login.css', TemplateView.as_view(template_name='login.css', content_type='text/css'), name='login_css'),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup_view, name="signup"),
]
