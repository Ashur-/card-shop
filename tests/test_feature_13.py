from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone

from .utils import Document


class FeatureTests(TestCase):
    def setUp(self):
        try:
            from decks.models import Deck  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models.Deck'")
        self.client = Client()
        self.noor_credentials = {"username": "noor", "password": "1234abcd."}
        self.noor = User.objects.create_user(**self.noor_credentials)
        self.alisha = User.objects.create_user(
            username="alisha", password="1234abcd."
        )
        self.client.post(reverse("login"), self.noor_credentials)
        self.deck = Deck.objects.create(
            name="ZZZZZZ", description="AAAAA"
        )
        self.deck.owner = self.noor
        self.deck.save()

    def test_deck_detail_returns_200(self):
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        self.assertEqual(
            response.status_code,
            200,
            msg="Did not get a 200 for deck details",
        )

    def test_deck_detail_shows_title(self):
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "ZZZZZZ",
            html.inner_text(),
            msg="Did not find the deck name on the page",
        )

    def test_deck_detail_with_no_cards_shows_message(self):
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "This deck has no cards",
            html.inner_text(),
            msg="Did not find the 'no cards' message on the page",
        )

    def test_deck_detail_with_a_cards_shows_card_name(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        card = Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            card.name,
            html.inner_text(),
            msg="Did not find the card name in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_card_start_date(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        card = Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            str(card.start_date.year),
            html.inner_text(),
            msg="Did not find the card start date in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_card_due_date(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        card = Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            str(card.due_date.year),
            html.inner_text(),
            msg="Did not find the card start date in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_assignee(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            self.noor.username,
            html.inner_text(),
            msg="Did not find the card start date in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_is_completed(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "no",
            html.inner_text(),
            msg="Did not find the card is completed in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_name_header(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "Name",
            html.inner_text(),
            msg="Did not find the header 'Name' in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_assignee_header(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "Assignee",
            html.inner_text(),
            msg="Did not find the header 'Assignee' in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_start_date_header(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "Start date",
            html.inner_text(),
            msg="Did not find the header 'Start date' in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_due_date_header(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "Due date",
            html.inner_text(),
            msg="Did not find the header 'Due date' in the detail page",
        )

    def test_deck_detail_with_a_cards_shows_is_completed_header(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "Is completed",
            html.inner_text(),
            msg="Did not find the header 'Is completed' in the detail page",
        )

    def test_deck_list_has_link_to_deck(self):
        path = reverse("list_decks")
        deck_path = reverse("show_deck", args=[self.deck.id])
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        children = html.get_all_children("a")
        detail_link = None
        for child in children:
            if child.attrs.get("href") == deck_path:
                detail_link = child
                break
        self.assertIsNotNone(
            detail_link,
            msg="Did not find the detail link for the deck in the page",
        )

    def test_deck_list_has_number_of_cards(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        path = reverse("list_decks")
        response = self.client.get(path)
        document = Document()
        document.feed(response.content.decode("utf-8"))
        html = document.select("html")
        self.assertIn(
            "1",
            html.inner_text(),
            msg="Did not find the number of cards for the deck in the page",
        )
