from django.test import TestCase, Client

from .utils import Document

# from decks.models import Deck


class FeatureTests(TestCase):
    def test_can_get_decks_urlpatterns(self):
        try:
            from decks.urls import urlpatterns  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find module 'decks.urls'")
        except ImportError:
            self.fail("Could not find 'decks.urls.urlpatterns'")

    def test_list_response_is_200(self):
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            self.assertEqual(
                response.status_code,
                200,
                msg="Did not get a 200 OK for the path decks/",
            )

    def test_page_has_fundamental_five(self):
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            self.assertTrue(
                document.has_fundamental_five(),
                msg="The response did not have the fundamental five",
            )

    def test_list_html_has_main_tag(self):
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            self.assertIsNotNone(
                document.select("html", "body", "main"),
                msg="The response did not have a main tag as a direct child of the body",  # noqa: E501
            )

    def test_main_tag_has_a_div_tag(self):
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            self.assertIsNotNone(
                document.select("html", "body", "main", "div"),
                msg="The response did not have a div tag as a direct child of the main",  # noqa: E501
            )

    def test_div_tag_has_an_h1_tag_with_content_my_decks(self):
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            h1 = document.select("html", "body", "main", "div", "h1")
            self.assertIsNotNone(
                h1,
                msg="The response did not have an h1 tag as a direct child of the div",  # noqa: E501
            )
            self.assertIn(
                "My Decks",
                h1.inner_text(),
                msg="h1 did not have content 'My Decks'",
            )

    def test_div_tag_has_a_p_tag_when_no_decks_with_message(self):
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            p = document.select("html", "body", "main", "div", "p")
            self.assertIsNotNone(
                p,
                msg="The response did not have a p tag as a direct child of the div",  # noqa: E501
            )
            self.assertIn(
                "You are not assigned to any decks",
                p.inner_text(),
                msg="p tag did not have content 'You are not assigned to any decks'",  # noqa: E501
            )

    def test_div_tag_has_a_table_with_headers_name_and_number(
        self,
    ):
        try:
            from decks.models import Deck  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models.Deck'")
        Deck.objects.bulk_create(
            [
                Deck(name="ZZZZZZ", description="AAAAA"),
            ]
        )
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            table = document.select("html", "body", "main", "div", "table")
            self.assertIsNotNone(
                table,
                msg="The response did not have a table tag as a direct child of the div",  # noqa: E501
            )
            self.assertIn(
                "Name",
                table.inner_text(),
                msg="table did not have 'Name' header in it",
            )
            self.assertIn(
                "Number of cards",
                table.inner_text(),
                msg="table did not have 'Number of cards' in it'",
            )

    def test_div_tag_has_a_table_tag_when_decks_exist_with_deck_names(
        self,
    ):
        try:
            from decks.models import Deck  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models.Deck'")
        Deck.objects.bulk_create(
            [
                Deck(name="ZZZZZZ", description="AAAAA"),
                Deck(name="YYYYYY", description="BBBBB"),
            ]
        )
        client = Client()
        response = client.get("/decks/")
        if (
            response.status_code != 302
            or not response.has_header("Location")
            or not response.headers.get("Location", "").startswith(
                "/accounts/login/"
            )
        ):
            content = response.content.decode("utf-8")
            document = Document()
            document.feed(content)
            table = document.select("html", "body", "main", "div", "table")
            self.assertIsNotNone(
                table,
                msg="The response did not have a table tag as a direct child of the div",  # noqa: E501
            )
            self.assertIn(
                "ZZZZZZ",
                table.inner_text(),
                msg="table did not have deck name in it",
            )
            self.assertIn(
                "YYYYYY",
                table.inner_text(),
                msg="table did not have deck name in it",
            )
