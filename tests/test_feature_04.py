from django.contrib import admin
from django.test import TestCase


class FeatureTests(TestCase):
    def test_deck_registered_with_admin(self):
        try:
            from decks.models import Deck

            self.assertTrue(
                admin.site.is_registered(Deck),
                msg="decks.models.Deck is not registered with the admin",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
