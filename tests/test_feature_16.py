from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone

from .utils import Document


class FeatureTests(TestCase):
    def setUp(self):
        try:
            from decks.models import Deck  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models.Deck'")
        self.client = Client()
        self.login()
        self.no_card_response = self.client.get("/cards/mine/")
        self.content = self.no_card_response.content.decode("utf-8")
        self.no_card_document = Document()
        self.no_card_document.feed(self.content)
        self.deck = Deck.objects.create(
            name="ZZZZZZ",
            description="AAAAAA",
            owner=self.noor,
        )
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        self.card = Card.objects.create(
            name="YYYYYY",
            start_date=timezone.now(),
            due_date=timezone.now(),
            deck=self.deck,
            assignee=self.noor,
        )
        self.card_response = self.client.get("/cards/mine/")
        self.content = self.card_response.content.decode("utf-8")
        self.card_document = Document()
        self.card_document.feed(self.content)

    def login(self):
        self.noor_credentials = {"username": "noor", "password": "1234abcd."}
        self.noor = User.objects.create_user(**self.noor_credentials)
        self.alisha = User.objects.create_user(
            username="alisha", password="1234abcd."
        )
        self.client.post(reverse("login"), self.noor_credentials)

    def test_show_my_cards_resolves_to_path(self):
        path = reverse("show_my_cards")
        self.assertEqual(
            path,
            "/cards/mine/",
            msg="The 'show_my_cards' did not resolve to the expected path",
        )

    def test_my_cards_with_no_cards_shows_message(self):
        html = self.no_card_document.select("html")
        self.assertIn(
            "You have no cards",
            html.inner_text(),
            msg="Did not find the message 'You have no cards'",
        )

    def test_my_cards_with_one_card_to_see_card_name(self):
        html = self.card_document.select("html")
        self.assertIn(
            "YYYYYY",
            html.inner_text(),
            msg="Did not find the name of the card assigned to the user",
        )

    def test_my_cards_with_one_card_to_see_card_status(self):
        html = self.card_document.select("html")
        self.assertNotIn(
            "Done",
            html.inner_text(),
            msg="Found the word 'Done' when I shouldn't have",
        )

    def test_my_cards_with_one_card_has_four_td_tags(self):
        html = self.card_document.select("html")
        cells = html.get_all_children("td")
        self.assertEqual(
            len(cells),
            4,
            msg="Should only have td tags for data rows",
        )

    def test_has_h1_tag_for_no_cards(self):
        h1 = self.no_card_document.select("html", "body", "main", "div", "h1")
        self.assertIsNotNone(
            h1,
            msg="Could not find h1 tag at html > body > main > div > h1",
        )
        self.assertIn(
            "My Cards",
            h1.inner_text(),
            msg="Could not find 'My Cards' in the h1 tag",
        )

    def test_has_h1_tag_for_cards(self):
        h1 = self.card_document.select("html", "body", "main", "div", "h1")
        self.assertIsNotNone(
            h1,
            msg="Could not find h1 tag at html > body > main > div > h1",
        )
        self.assertIn(
            "My Cards",
            h1.inner_text(),
            msg="Could not find 'My Cards' in the h1 tag",
        )
