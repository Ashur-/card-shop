from django.contrib import admin
from django.test import TestCase


class FeatureTests(TestCase):
    def test_deck_registered_with_admin(self):
        try:
            from cards.models import Card

            self.assertTrue(
                admin.site.is_registered(Card),
                msg="cards.models.Card is not registered with the admin",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
