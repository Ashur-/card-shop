from django.test import SimpleTestCase


class FeatureTests(SimpleTestCase):
    def test_tracker_deck_created(self):
        try:
            from tracker.settings import INSTALLED_APPS  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find the Django deck 'tracker'")

    def test_accounts_app_created(self):
        try:
            from accounts.apps import AccountsConfig  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find the Django app 'accounts'")

    def test_decks_app_created(self):
        try:
            from decks.apps import DecksConfig  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find the Django app 'decks'")

    def test_cards_app_created(self):
        try:
            from cards.apps import CardsConfig  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find the Django app 'cards'")

    def test_accounts_app_installed(self):
        try:
            from tracker.settings import INSTALLED_APPS

            self.assertIn("accounts.apps.AccountsConfig", INSTALLED_APPS)
        except ModuleNotFoundError:
            self.fail("Could not find 'accounts' installed in 'tracker'")

    def test_decks_app_installed(self):
        try:
            from tracker.settings import INSTALLED_APPS

            self.assertIn("decks.apps.DecksConfig", INSTALLED_APPS)
        except ModuleNotFoundError:
            self.fail("Could not find 'decks' installed in 'tracker'")

    def test_cards_app_installed(self):
        try:
            from tracker.settings import INSTALLED_APPS

            self.assertIn("cards.apps.CardsConfig", INSTALLED_APPS)
        except ModuleNotFoundError:
            self.fail("Could not find 'cards' installed in 'tracker'")
