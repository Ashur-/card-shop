from django.test import TestCase
from django.db import models
from django.db.utils import OperationalError
from django.contrib.auth.models import User


class FeatureTests(TestCase):
    def test_deck_model_exists(self):
        try:
            from decks.models import Deck  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models.Deck'")

    def test_deck_model_has_char_name_field(self):
        try:
            from decks.models import Deck

            name = Deck.name
            self.assertIsInstance(
                name.field,
                models.CharField,
                msg="Deck.name should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.name'")

    def test_deck_model_has_name_with_max_length_200_characters(self):
        try:
            from decks.models import Deck

            name = Deck.name
            self.assertEqual(
                name.field.max_length,
                200,
                msg="The max length of Deck.name should be 200",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.name'")

    def test_deck_model_has_name_that_is_not_nullable(self):
        try:
            from decks.models import Deck

            name = Deck.name
            self.assertFalse(
                name.field.null,
                msg="Deck.name should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.name'")

    def test_deck_model_has_name_that_is_not_blank(self):
        try:
            from decks.models import Deck

            name = Deck.name
            self.assertFalse(
                name.field.blank,
                msg="Deck.name should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.name'")

    def test_deck_model_has_text_description_field(self):
        try:
            from decks.models import Deck

            description = Deck.description
            self.assertIsInstance(
                description.field,
                models.TextField,
                msg="Deck.description should be a text field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.description'")

    def test_deck_model_has_description_that_is_not_nullable(self):
        try:
            from decks.models import Deck

            description = Deck.description
            self.assertFalse(
                description.field.null,
                msg="Deck.description should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.description'")

    def test_deck_model_has_description_that_is_cannot_be_blank(self):
        try:
            from decks.models import Deck

            description = Deck.description
            self.assertFalse(
                description.field.blank,
                msg="Deck.description should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.description'")

    def test_deck_model_has_an_owner(self):
        try:
            from decks.models import Deck

            owner = Deck.owner
            self.assertIsInstance(
                owner.field,
                models.ForeignKey,
                msg="Deck.owner should be a foreign key field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.owner'")

    def test_deck_model_has_owner_related_name_of_decks(self):
        try:
            from decks.models import Deck

            owner = Deck.owner
            self.assertEqual(
                owner.field.related_query_name(),
                "decks",
                msg="Deck.owner should have a related name of 'decks'",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.owner'")

    def test_deck_model_has_owner_related_to_auth_user(self):
        try:
            from django.contrib.auth.models import User
            from decks.models import Deck

            owner = Deck.owner
            self.assertEqual(
                owner.field.related_model,
                User,
                msg="Deck.owner should be related to the 'auth.User' model",  # noqa: E501
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.owner'")

    def test_deck_model_has_nullable_owner(self):
        try:
            from decks.models import Deck

            owner = Deck.owner
            self.assertEqual(
                owner.field.null,
                True,
                msg="Deck.owner should be nullable",  # noqa: E501
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.owner'")

    def test_deck_can_create_decks(self):
        try:
            from decks.models import Deck

            user = User.objects.create_user("testuser")

            try:
                Deck.objects.create(
                    name="Test Deck",
                    description="Test Description",
                    owner=user,
                )
            except OperationalError:
                self.fail(
                    "Could not create a deck because there was no database table"
                )

        except ModuleNotFoundError:
            self.fail("Could not find 'decks.models'")
        except ImportError:
            self.fail("Could not find 'decks.models.Deck'")
        except AttributeError:
            self.fail("Could not find 'Deck.owner'")
