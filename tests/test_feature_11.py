from django.test import TestCase
from django.db import models
from django.utils import timezone
from django.db.utils import OperationalError
from django.contrib.auth.models import User


class FeatureTests(TestCase):
    def test_card_model_exists(self):
        try:
            from cards.models import Card  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")

    def test_card_model_has_char_name_field(self):
        try:
            from cards.models import Card

            name = Card.name
            self.assertIsInstance(
                name.field,
                models.CharField,
                msg="Card.name should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.name'")

    def test_card_model_has_name_with_max_length_200_characters(self):
        try:
            from cards.models import Card

            name = Card.name
            self.assertEqual(
                name.field.max_length,
                200,
                msg="The max length of Card.name should be 200",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.name'")

    def test_card_model_has_name_that_is_not_nullable(self):
        try:
            from cards.models import Card

            name = Card.name
            self.assertFalse(
                name.field.null,
                msg="Card.name should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.name'")

    def test_card_model_has_name_that_is_not_blank(self):
        try:
            from cards.models import Card

            name = Card.name
            self.assertFalse(
                name.field.blank,
                msg="Card.name should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.name'")

    def test_card_model_has_date_time_start_date_field(self):
        try:
            from cards.models import Card

            start_date = Card.start_date
            self.assertIsInstance(
                start_date.field,
                models.DateTimeField,
                msg="Card.start_date should be a date-time field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.start_date'")

    def test_card_model_has_start_date_that_is_not_nullable(self):
        try:
            from cards.models import Card

            start_date = Card.start_date
            self.assertFalse(
                start_date.field.null,
                msg="Card.start_date should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.start_date'")

    def test_card_model_has_start_date_that_is_cannot_be_blank(self):
        try:
            from cards.models import Card

            start_date = Card.start_date
            self.assertFalse(
                start_date.field.blank,
                msg="Card.start_date should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.start_date'")

    def test_card_model_has_date_time_due_date_field(self):
        try:
            from cards.models import Card

            due_date = Card.due_date
            self.assertIsInstance(
                due_date.field,
                models.DateTimeField,
                msg="Card.due_date should be a date-time field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.due_date'")

    def test_card_model_has_due_date_that_is_not_nullable(self):
        try:
            from cards.models import Card

            due_date = Card.due_date
            self.assertFalse(
                due_date.field.null,
                msg="Card.due_date should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.due_date'")

    def test_card_model_has_due_date_that_is_cannot_be_blank(self):
        try:
            from cards.models import Card

            due_date = Card.due_date
            self.assertFalse(
                due_date.field.blank,
                msg="Card.due_date should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.due_date'")

    def test_card_model_has_deck_foreign_key_field(self):
        try:
            from cards.models import Card

            deck = Card.deck
            self.assertIsInstance(
                deck.field,
                models.ForeignKey,
                msg="Card.deck should be a foreign key field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.deck'")

    def test_card_model_has_deck_related_name_of_cards(self):
        try:
            from cards.models import Card

            deck = Card.deck
            self.assertEqual(
                deck.field.related_query_name(),
                "cards",
                msg="Card.deck should have a related name of 'cards'",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.deck'")

    def test_card_model_has_deck_related_to_decks_deck(self):
        try:
            from decks.models import Deck
            from cards.models import Card

            deck = Card.deck
            self.assertEqual(
                deck.field.related_model,
                Deck,
                msg="Card.deck should be related to the 'decks.Deck' model",  # noqa: E501
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.deck'")

    def test_card_model_has_deck_has_on_delete_cascade(self):
        try:
            from cards.models import Card

            deck = Card.deck
            self.assertEqual(
                deck.field.remote_field.on_delete,
                models.CASCADE,
                msg="Card.deck should have CASCADE for on delete",  # noqa: E501
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.deck'")

    def test_card_model_has_assignee_foreign_key_field(self):
        try:
            from cards.models import Card

            assignee = Card.assignee
            self.assertIsInstance(
                assignee.field,
                models.ForeignKey,
                msg="Card.assignee should be a foreign key field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.assignee'")

    def test_card_model_has_assignee_related_name_of_cards(self):
        try:
            from cards.models import Card

            assignee = Card.assignee
            self.assertEqual(
                assignee.field.related_query_name(),
                "cards",
                msg="Card.assignee should have a related name of 'cards'",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.assignee'")

    def test_card_model_has_assignee_related_to_auth_user(self):
        try:
            from django.contrib.auth.models import User
            from cards.models import Card

            assignee = Card.assignee
            self.assertEqual(
                assignee.field.related_model,
                User,
                msg="Card.assignee should be related to the 'auth.User' model",  # noqa: E501
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.assignee'")

    def test_card_model_has_assignee_has_on_delete_set_null(self):
        try:
            from cards.models import Card

            assignee = Card.assignee
            self.assertEqual(
                assignee.field.remote_field.on_delete,
                models.CASCADE,
                msg="Card.assignee should have CASCADE for on delete",  # noqa: E501
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Card'")
        except AttributeError:
            self.fail("Could not find 'Card.assignee'")

    def test_card_can_create_cards(self):
        try:
            from decks.models import Deck
            from cards.models import Card

            user = User.objects.create_user("testuser")
            deck = Deck.objects.create(
                name="Test Deck",
                description="Test Description",
                owner=user,
            )
            try:
                Card.objects.create(
                    name="Test Card",
                    start_date=timezone.now(),
                    due_date=timezone.now(),
                    deck=deck,
                    assignee=user,
                )
            except OperationalError:
                self.fail(
                    "Could not create a card because there was no database table"
                )

        except ModuleNotFoundError:
            self.fail("Could not find 'cards.models'")
        except ImportError:
            self.fail("Could not find 'cards.models.Deck'")
